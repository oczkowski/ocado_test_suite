### Ocado BDD Test Suite ###

If you would like to download the test suite from repository, please follow the steps:

1. Download and install GIT control version system. If you are a Linux user, you probably have GIT already installed. To check it, please run this line in Linux terminal: git --version.Proper version number should be displayed if it exists. If not, there should be an error message. To install it use your package manager. For Debian, Ubuntu and Mint distros simply run this command as a root: 'apt-get install git' and confirm if needed.
As a Windows user you need to install one of GIT implementations. TortoiseGIT is suggested one.
2. To download test suite create an empty directory for it and:
	- Linux users: open terminal with newly created directory and put in it: 'git clone https://oczkowski@bitbucket.org/oczkowski/ocado_test_suite.git' and press Enter
	- Windows user: do a right-click on opened directory and choose Git Clone option from TortoiseGit menu section. Then Paste 'git clone https://oczkowski@bitbucket.org/oczkowski/ocado_test_suite.git' address to URL section and click OK.
3. Repository should be downloaded. You should see newly created directory full of files.
4. Install Java 7 JDK for your distribution. Windows installer has graphical interface and is easy to run. Linux users probably have Java JDK already installed. Run 'java --version' in terminal to checkthe version. If it does not exist or version older than 7 is installed please install proper packages using package manager or adding customized packages links to your manager.
5. Install Maven tool. As a Linux user check if it is installed by typing 'mvn -v' in terminal. To install it use your package manager (apt-get install maven' in Debian-like distros). As a Windows user please follow these steps: https://maven.apache.org/guides/getting-started/windows-prerequisites.html
6. Check if Maven is properly installed by putting 'mvn -v' in terminal or Windows command prompt. If not, please follow step 4 and check if you did as expected.
7. Go to ocado-test-suite and bdd-test-suite directory in terminal or command prompt and look for a pom.xml file. To display all files in current directory, please put 'ls -la' command for Linux and 'dir' for Windows.
8. If there is a pom.xml file visible, you can now run test suite by pressing: 'mvn clean test -Dtest=AllTests'.
9. Now there should be lots of Maven messages displaying in the command line and Firefox should be opened after that.
10. Please do not move mouse or clickanything in the browser, because it may disturb or fail tests.
11. Firefox should be closed when all test scripts finish their work. Now you see BUILD SUCCESS or BUILD FAILED message. You should see quantity and names of failed tests if they do exist. Error messages should be dislpayed as well.

Two of test cases might be failing because of temporary absence of shopping trolley in Ocado webpage. This might occur only when user is logged out.
