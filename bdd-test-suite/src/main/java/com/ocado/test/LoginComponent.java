package com.ocado.test;

import com.ocado.test.utils.WaitFor;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static com.ocado.test.xpaths.LoginComponentXpaths.*;

/**
 * Created by kuba on 9/5/15.
 */
public class LoginComponent {

    private WebDriver driver;

    @FindBy(xpath = LOGIN_COMPONENT_USERNAME_LOGIN_FIELDBOX)
    public WebElement usernameFieldbox;

    @FindBy(xpath = LOGIN_COMPONENT_PASSWORD_FIELDBOX)
    public WebElement passwordFieldbox;

    @FindBy(xpath = LOGIN_COMPONENT_LOGIN_BUTTON)
    public WebElement loginButton;

    public LoginComponent(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    // this one allows enter password when user is going to log in
    public LoginComponent enterUsername(String username) {
        WaitFor.elementVisibleAndDisplayed(driver, usernameFieldbox);
        usernameFieldbox.sendKeys(username);
        return this;
    }

    // enters mandatory password
    public LoginComponent enterPassword(String password) {
        WaitFor.elementVisibleAndDisplayed(driver, passwordFieldbox);
        passwordFieldbox.sendKeys(password);
        return this;
    }

    // clicks Login button to confirm
    public MainBar clickLoginButton() {
        loginButton.click();
        WaitFor.elementNotVisible(driver, By.xpath(LOGIN_COMPONENT_POPUP_WINDOW));
        return new MainBar(driver);
    }
}
