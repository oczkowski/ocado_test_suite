package com.ocado.test.xpaths;

/**
 * Created by kuba on 9/5/15.
 */
public class BrowseShopXpaths {
    public static final String BROWSE_SHOP_PAGE_CONTENT = "//div[@id='content']";
    public static final String BROWSE_SHOP_PAGE_YOU_ARE_IN_CURRENT_PAGE = "//ul[@id='breadcrumb']//h1[text()='Shop']";
}
