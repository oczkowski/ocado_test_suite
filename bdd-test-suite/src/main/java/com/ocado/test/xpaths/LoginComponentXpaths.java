package com.ocado.test.xpaths;

/**
 * Created by kuba on 9/5/15.
 */
public class LoginComponentXpaths {
    public static final String LOGIN_COMPONENT_POPUP_WINDOW = "//div[@id='loginRegPopup']/div[contains(@class, 'popup')]";
    public static final String LOGIN_COMPONENT_USERNAME_LOGIN_FIELDBOX = LOGIN_COMPONENT_POPUP_WINDOW + "//input[@id='username']";
    public static final String LOGIN_COMPONENT_PASSWORD_FIELDBOX = LOGIN_COMPONENT_POPUP_WINDOW + "//input[@id='password']";
    public static final String LOGIN_COMPONENT_LOGIN_BUTTON = "//button[@id='js-popupLoginButton']";
}
