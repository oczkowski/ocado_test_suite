package com.ocado.test.xpaths;

/**
 * Created by kuba on 9/6/15.
 */
public class ViewTrolleyPageXpaths {
    public static final String VIEW_TROLLEY_PAGE_YOU_ARE_IN_CURRENT_PAGE = "//ul[@id='breadcrumb']//h1[text()='Your trolley']";
    public static final String VIEW_TROLLEY_EMPTY_BASKET = "//a[@class='emptyBasket']";
    public static final String VIEW_TROLLEY_EMPTY_BASKET_POP_UP = "//div[contains(@id, 'confirmcancel')]";
    public static final String VIEW_TROLLEY_EMPTY_BASKET_POP_UP_EMPTY_BUTTON = "//button[contains(@id, 'empty-trolley')]";
}
