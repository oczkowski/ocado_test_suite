package com.ocado.test.xpaths;

/**
 * Created by kuba on 9/5/15.
 */
public class MainBarXpaths {
    public static final String MAIN_BAR = "//div[@id='fix']";
    public static final String MAIN_BAR_PRIMARY_BAR = MAIN_BAR + "//div[@id='primaryBar']";
    public static final String MAIN_BAR_PRIMARY_BAR_BROWSE_SHOP_LINK = MAIN_BAR_PRIMARY_BAR + "//div[@class='browseShop']/a";
    public static final String MAIN_BAR_PRIMARY_BAR_OFFERS_LINK = MAIN_BAR_PRIMARY_BAR + "//li[@id='offers']/a";
    public static final String MAIN_BAR_PRIMARY_BAR_SITE_TABS_LIST = "//ul[@id='siteTabs']";
    public static final String MAIN_BAR_LOGIN_BUTTON = "//a[@id='loginButton']";
    public static final String MAIN_BAR_VIEW_TROLLEY_BUTTON = MAIN_BAR + "//a[contains(@class, 'viewTrolley')]";
    public static final String MAIN_BAR_MY_OCADO_LINK = MAIN_BAR + "//div[@class='browseMyShop']/a";
    public static final String MAIN_BAR_MY_OCADO_DROPDOWN = MAIN_BAR + "//div[contains(@class, 'dropDownNav')]";
    public static final String MAIN_BAR_MY_OCADO_LOGOUT_LINK = MAIN_BAR_MY_OCADO_DROPDOWN + "//a[@class='Log-out']";
    public static final String MAIN_BAR_BASKET_SUMMARY = "//div[@id='basketSummary']//span[@id='basketSummaryTotal']";
}
