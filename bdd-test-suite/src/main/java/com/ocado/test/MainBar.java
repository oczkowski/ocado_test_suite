package com.ocado.test;

import com.ocado.test.utils.WaitFor;
import com.ocado.test.utils.WebUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static com.ocado.test.xpaths.MainBarXpaths.*;
import static com.ocado.test.xpaths.BrowseShopXpaths.*;
import static com.ocado.test.xpaths.OffersPageXpaths.*;
import static com.ocado.test.xpaths.LoginComponentXpaths.*;
import static com.ocado.test.xpaths.ViewTrolleyPageXpaths.*;
import static com.ocado.test.xpaths.MainPageXpaths.*;

/**
 * Created by kuba on 9/5/15.
 */
public class MainBar {

    private WebDriver driver;

    WebUtils webUtils;

    @FindBy(xpath = MAIN_BAR_PRIMARY_BAR_BROWSE_SHOP_LINK)
    public WebElement browseShopLink;

    @FindBy(xpath = MAIN_BAR_PRIMARY_BAR_OFFERS_LINK)
    public WebElement offersLink;

    @FindBy(xpath = MAIN_BAR_LOGIN_BUTTON)
    public WebElement loginButton;

    public MainBar(WebDriver driver) {
        this.driver = driver;
        webUtils = new WebUtils(driver);
        PageFactory.initElements(driver, this);
    }

    // redirects user to Browse Shop Page
    public BrowseShopPage goToBrowseShopPage() {
        WaitFor.elementVisibleAndDisplayed(driver, browseShopLink);
        browseShopLink.click();
        WaitFor.elementVisibleAndDisplayed(driver, By.xpath(BROWSE_SHOP_PAGE_YOU_ARE_IN_CURRENT_PAGE));
        return new BrowseShopPage();
    }

    // redirects to Offers page
    public OffersPage goToOffersPage() {
        offersLink.click();
        WaitFor.elementVisibleAndDisplayed(driver, By.xpath(OFFERS_PAGE_YOU_ARE_IN_CURRENT_PAGE));
        return new OffersPage(driver);
    }

    // opens login component
    public LoginComponent openLoginComponent() {
        loginButton.click();
        WaitFor.elementVisibleAndDisplayed(driver, By.xpath(LOGIN_COMPONENT_POPUP_WINDOW));
        return new LoginComponent(driver);
    }

    // gets user to Trolley
    public ViewTrolleyPage viewTrolley() {
        driver.findElement(By.xpath(MAIN_BAR_VIEW_TROLLEY_BUTTON)).click();
        WaitFor.elementVisibleAndDisplayed(driver, By.xpath(VIEW_TROLLEY_PAGE_YOU_ARE_IN_CURRENT_PAGE));
        return new ViewTrolleyPage(driver);
    }

    // allows expand and show My Ocado dropdown when user is currently logged in
    // it performs mouse actions on the webpage
    public MainBar expandMyOcadoDropdown() {
        Actions actions = new Actions(driver);
        actions.moveToElement(driver.findElement(By.xpath(MAIN_BAR_MY_OCADO_LINK))).perform();
        WaitFor.elementVisibleAndDisplayed(driver, (By.xpath(MAIN_BAR_MY_OCADO_DROPDOWN)));
        return this;
    }

    // logouts user when My Ocado dropdown is expaned
    public MainPage logOutCustomer() {
        driver.findElement(By.xpath(MAIN_BAR_MY_OCADO_LOGOUT_LINK)).click();
        try{
            WaitFor.elementVisibleAndDisplayed(driver, By.xpath(MAIN_PAGE_WELCOME_BOX));
        } catch (org.openqa.selenium.TimeoutException e) {
            webUtils.tryToLoadMainPage(System.getProperty("ocado.url"));
        }
        return new MainPage(driver);
    }
}
