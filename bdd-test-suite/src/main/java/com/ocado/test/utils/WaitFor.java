package com.ocado.test.utils;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;

import java.util.concurrent.TimeUnit;

/**
 * Created by kuba on 9/5/15.
 */
public class WaitFor {

    // global wait parameters can be modified here
    private static final int timeout = 10;
    private static final int interval = 100;

    public static void pageToBeLoaded() {

    }

    public static WebElement elementVisibleAndDisplayed(WebDriver driver, By locator) {
        FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(timeout, TimeUnit.SECONDS)
                .pollingEvery(interval, TimeUnit.MILLISECONDS)
                .ignoring(NoSuchElementException.class);
        return wait.until(ExpectedConditions.visibilityOf(driver.findElement(locator)));
    }

    public static WebElement elementVisibleAndDisplayed(WebDriver driver, WebElement element) {
        FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(timeout, TimeUnit.SECONDS)
                .pollingEvery(interval, TimeUnit.MILLISECONDS)
                .ignoring(NoSuchElementException.class);
        return wait.until(ExpectedConditions.visibilityOf(element));
    }

    public static Boolean elementNotVisible(WebDriver driver, By locator) {
        FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(timeout, TimeUnit.SECONDS)
                .pollingEvery(interval, TimeUnit.MILLISECONDS)
                .ignoring(NoSuchElementException.class);
        return wait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
    }

    public static Boolean isWebElementAttachedToDom(WebDriver driver, By locator) {
        FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(timeout, TimeUnit.SECONDS)
                .pollingEvery(interval, TimeUnit.MILLISECONDS)
                .ignoring(StaleElementReferenceException.class);
        return wait.until(ExpectedConditions.not(ExpectedConditions.stalenessOf(driver.findElement(locator))));
    }
}
