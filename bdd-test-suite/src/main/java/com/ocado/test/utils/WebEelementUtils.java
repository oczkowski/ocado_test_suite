package com.ocado.test.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by kuba on 9/5/15.
 */
public class WebEelementUtils {
    public static WebElement findElementAndWaitForDom(WebDriver driver, By locator) {
        try {
            driver.findElement(locator);
        } catch (StaleElementReferenceException e) {
            WaitFor.isWebElementAttachedToDom(driver, locator);
        }
        return driver.findElement(locator);
    }
}
