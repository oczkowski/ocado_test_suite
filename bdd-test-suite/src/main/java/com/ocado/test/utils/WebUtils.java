package com.ocado.test.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static com.ocado.test.xpaths.MainBarXpaths.*;
import static org.assertj.core.api.Assertions.fail;

/**
 * Created by kuba on 9/6/15.
 */
public class WebUtils {

    private WebDriver driver;

    public WebUtils(WebDriver driver) {
        this.driver = driver;
    }

    public static Boolean isCustomerLoggedIn(WebDriver driver){
        return driver.findElements(By.xpath(MAIN_BAR_MY_OCADO_LINK)).size() > 0;
    }

    public static Boolean isCustomerLoggedOut(WebDriver driver) {
        return driver.findElements(By.xpath(MAIN_BAR_MY_OCADO_LINK)).size() == 0;
    }

    public void tryToLoadMainPage(String url) {
        if (isTabsListDisplayed() == false) {
            for (int i = 0; i <= 5 && isTabsListDisplayed() == false; i++) {
                driver.get(System.getProperty(url));
            }
            if (isTabsListDisplayed() == true) {
                return;
            } else fail("No proper main page loaded. Tried 5 times.");
        }
    }

    protected Boolean isTabsListDisplayed() {
        return driver.findElements(By.xpath(MAIN_BAR_PRIMARY_BAR_SITE_TABS_LIST)).size() > 0;
    }

    public static Double convertPriceStringToInt(String price) {
        return Double.parseDouble(price.substring(1, price.length()));
    }
}
