package com.ocado.test;

import com.ocado.test.utils.WaitFor;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static com.ocado.test.xpaths.ViewTrolleyPageXpaths.*;

/**
 * Created by kuba on 9/6/15.
 */
public class ViewTrolleyPage {

    private WebDriver driver;

    @FindBy(xpath = VIEW_TROLLEY_EMPTY_BASKET)
    public WebElement emptyTrolley;

    @FindBy(xpath = VIEW_TROLLEY_EMPTY_BASKET_POP_UP_EMPTY_BUTTON)
    public WebElement confirmEmptyTrolleyButton;

    public ViewTrolleyPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    // clicks Empty Trolley button on Main Bar
    public ViewTrolleyPage clickEmptyTrolley() {
        emptyTrolley.click();
        WaitFor.elementVisibleAndDisplayed(driver, By.xpath(VIEW_TROLLEY_EMPTY_BASKET_POP_UP));
        return this;
    }

    // confirms emptying trolley when proper pop-up is displayed
    public ViewTrolleyPage clickConfirmEmptyTrolley() {
        confirmEmptyTrolleyButton.click();
        WaitFor.elementNotVisible(driver, By.xpath(VIEW_TROLLEY_EMPTY_BASKET_POP_UP));
        return this;
    }
}
