package com.ocado.test;

import com.ocado.test.utils.WebUtils;
import cucumber.api.CucumberOptions;
import cucumber.api.testng.CucumberFeatureWrapper;
import cucumber.api.testng.TestNGCucumberRunner;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;

import static com.ocado.test.xpaths.ViewTrolleyPageXpaths.*;

/**
 * Created by kuba on 9/6/15.
 */
@CucumberOptions(format = "json:target/cucumber-report-feature-composite.json",
        features = "src/test/resources/com/ocado/test/"/*,
        glue = "src/test/java/com/ocado/test/"*/)
public class AllTests extends TestInit {
    private TestNGCucumberRunner testNGCucumberRunner;
    protected WebDriver driver = WebDriverInstance.getDriver();

    @BeforeClass(alwaysRun = true)
    public void setUpClass() throws Exception {
        testNGCucumberRunner = new TestNGCucumberRunner(this.getClass());
    }

    // the launcher
    @Test(groups = "", description = "Runs Ocado Tests", dataProvider = "features")
    public void run(CucumberFeatureWrapper cucumberFeature) {
        testNGCucumberRunner.runCucumber(cucumberFeature.getCucumberFeature());
    }

    // this one cleans trolley content and log outs if needed after every fired test
    @AfterMethod(alwaysRun = true)
    public void logout() {
        MainBar mainBar = new MainBar(driver);
        ViewTrolleyPage viewTrolleyPage = new ViewTrolleyPage(driver);

        if(WebUtils.isCustomerLoggedIn(driver)) {
            mainBar.viewTrolley();
            if (driver.findElement(By.xpath(VIEW_TROLLEY_EMPTY_BASKET)).isDisplayed()) {
                viewTrolleyPage.clickEmptyTrolley();
                viewTrolleyPage.clickConfirmEmptyTrolley();
            }
            mainBar.expandMyOcadoDropdown();
            mainBar.logOutCustomer();
        }
        mainBar.viewTrolley();
        if (driver.findElement(By.xpath(VIEW_TROLLEY_EMPTY_BASKET)).isDisplayed()) {
            viewTrolleyPage.clickEmptyTrolley();
            viewTrolleyPage.clickConfirmEmptyTrolley();
        }
    }

    @DataProvider
    public Object[][] features() {
        return testNGCucumberRunner.provideFeatures();
    }


}
