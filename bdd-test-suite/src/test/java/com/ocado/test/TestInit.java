package com.ocado.test;


import com.ocado.test.utils.WebUtils;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import static com.ocado.test.xpaths.MainBarXpaths.MAIN_BAR_PRIMARY_BAR_SITE_TABS_LIST;

/**
 * Base class for Ocado BDD tests.
 */
public class TestInit {

    private WebDriver driver;

    WebUtils webUtils;

    // URL needs to be set first and then Webdriver instance should be returned
    public TestInit() {
        setUrl();
        instantiateDriver();
        webUtils = new WebUtils(driver);
    }

    // opens expected URL and maximizes window shortly after opening the browser
    @BeforeTest(alwaysRun = true)
    protected void initialize() throws Exception {
        openOcadoUrl();
        maximizeWindow();
    }

    // go to Main Page after every one test has been finished
    @AfterMethod(alwaysRun = true)
    protected void goBackToMainPage() throws Exception {
        openOcadoUrl();
    }

    // quits Webdriver when all work is finished
    @AfterTest(alwaysRun = true)
    protected void quit() {
        driver.quit();
    }

    // gets instance of Webdriver to launch it
    private void instantiateDriver() {
        driver = WebDriverInstance.instantiate();
        //driver = new FirefoxDriver();
    }

    // gets to desired URL
    private void openOcadoUrl() {
        driver.get(getUrl());
        webUtils.tryToLoadMainPage(getUrl());
    }

    // it sets URL
    // can be put using Maven property or can set here when null
    private void setUrl() {
        if (System.getProperty("ocado.url") == null) {
            System.setProperty("ocado.url", "https://www.ocado.com/webshop/startWebshop.do?clkInTab=home");
        }
    }

    // returns previously set URL
    protected String getUrl() {
        return System.getProperty("ocado.url");
    }

    // returns current Webdriver instance
    public WebDriver getDriver() {
        return driver;
    }

    // maximizes browser window
    private void maximizeWindow() {
        driver.manage().window().maximize();
    }

}
