package com.ocado.test;

import com.ocado.test.utils.WebUtils;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;
import static org.assertj.core.api.Assertions.*;

/**
 * Created by kuba on 9/8/15.
 */
public class SavingsFeatureDisplayCorrectValueSteps {

    protected WebDriver driver = WebDriverInstance.getDriver();
    MainBar mainBar;
    Double saving;
    Double wasPrice;
    Double isPrice;

    @Given("^a customer on Browse Shop$")
    public void aCustomerOnBrowseShopPage() {
        mainBar = new MainBar(driver);

        // redirect to Browse Shop pgae
        mainBar.goToBrowseShopPage();

    }

    @When("^customer add item in Half price promo to basket$")
    public void customerAddItemInHalfPricePromoToBasket() {
        // find items on Half Price promo and choose the first one
        List<WebElement> itemsOnHalfPrice = driver.findElements(By.xpath("//p[@class='onOffer']//span[contains(text(), 'Half Price')]"));
        driver.findElement(By.xpath("(//p[@class='onOffer']//span[contains(text(), 'Half Price')]/../../../..//input[@class='productAdd'])[1]"))
                .click();

        // collect regular and current prices of the item
        wasPrice = WebUtils.convertPriceStringToInt(driver.findElement(By
                .xpath("(//p[@class='onOffer']//span[contains(text(), 'Half Price')]/../../../..//span[@class='wasPrice'])[1]")).getText());
        isPrice = WebUtils.convertPriceStringToInt(driver.findElement(By
                .xpath("(//p[@class='onOffer']//span[contains(text(), 'Half Price')]/../../../..//span[@class='nowPrice'])[1]")).getText());

    }

    @Then("^saving field is updated$")
    public void savingFieldIsUpdated() {
        // check if saving is displayed and collect it
        assertThat(driver.findElement(By.xpath("//span[@id='savings']/span")).isDisplayed())
                .isTrue();
        saving = WebUtils.convertPriceStringToInt(driver.findElement(By.xpath("//span[@id='savings']/span")).getText());
    }

    @And("^contains correct savings value$")
    public void containsCorrectSavingsValue() {
        // assert if calculated saving is real discrepancy between regular and promo price
        assertThat(saving)
                .isEqualTo(wasPrice - isPrice);
    }
}
