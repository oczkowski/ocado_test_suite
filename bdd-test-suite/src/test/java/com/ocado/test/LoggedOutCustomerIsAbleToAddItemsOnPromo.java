package com.ocado.test;

import com.ocado.test.utils.WebUtils;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

import static com.ocado.test.xpaths.MainBarXpaths.MAIN_BAR_BASKET_SUMMARY;
import static org.assertj.core.api.Assertions.*;

/**
 * Created by kuba on 9/6/15.
 */
public class LoggedOutCustomerIsAbleToAddItemsOnPromo {

    protected WebDriver driver = WebDriverInstance.getDriver();

    private int anyNumber;
    private int forNumber;

    @Given("^a customer on Browse Shop page$")
    public void aCustomerOnBrowseShopPage() {
        MainBar mainBar = new MainBar(driver);

        mainBar.goToBrowseShopPage();

    }

    @When("^'Buy any XX for XX' promo is available$")
    public void buyAnyForPromoIsAvailable() {
        List<WebElement> itemsOnOffer = driver
                .findElements(By.xpath("//p[@class='onOffer']//span[contains(text(), 'Buy any') and contains(text(), 'for')]"));
        String firstItemOnOffer = itemsOnOffer.get(0).getText();

        String[] offer = firstItemOnOffer.replaceAll("\\D+"," ").substring(1).split("\\s+");
        //System.out.println(offer[0] + offer[1]);
        anyNumber = Integer.parseInt(offer[0]);
        forNumber = Integer.parseInt(offer[1]);

    }

    @And("^promotion conditions are met$")
    public void promotionConditionsAreMet() {
        driver.findElement(By
                .xpath("(//p[@class='onOffer']//span[contains(text(), 'Buy any') and contains(text(), 'for')]/../../../..//input[@class='quantity'])[1]"))
                .sendKeys(Keys.chord((Keys.CONTROL), "a"), Integer.toString(anyNumber));

        driver.findElement(By
                .xpath("(//p[@class='onOffer']//span[contains(text(), 'Buy any') and contains(text(), 'for')]/../../../..//input[@class='productAdd'])[1]"))
                .click();

        System.out.println(driver.findElement(By.xpath("(" + MAIN_BAR_BASKET_SUMMARY + ")[1]")).getText());
    }

    @Then("^total amount in basket is calculated correct$")
    public void totalAmountInBasketIsCalculatedCorrect() {
        assertThat(WebUtils.convertPriceStringToInt(driver.findElement(By.xpath("(" + MAIN_BAR_BASKET_SUMMARY + ")[1]")).getText()))
                .isEqualTo(forNumber);
    }
}
