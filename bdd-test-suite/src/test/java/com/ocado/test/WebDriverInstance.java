package com.ocado.test;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * Created by kuba on 9/7/15.
 */
public class WebDriverInstance {
    private static WebDriver driver;

    // launches and returns new WebDriver instance
    public static WebDriver instantiate() {
        driver = new FirefoxDriver();
        return driver;
    }

    // returns current WebDriver instance
    public static WebDriver getDriver() {
        return driver;
    }
}
