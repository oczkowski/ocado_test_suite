package com.ocado.test;

import com.ocado.test.utils.WebUtils;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.*;
import static com.ocado.test.xpaths.MainBarXpaths.*;


/**
 * Created by kuba on 9/6/15.
 */
public class LoggedInCustomerItemsOnPromoSteps {

    protected WebDriver driver = WebDriverInstance.getDriver();

    MainBar mainBar;
    LoginComponent loginComponent;

    @Given("^a logged customer$")
    public void aLoggedInCustomer() {
        mainBar = new MainBar(driver);
        loginComponent = new LoginComponent(driver);

        // get to login component and login as a regular customer
        mainBar.openLoginComponent();
        loginComponent.enterUsername("jane.doe80@wp.pl");
        loginComponent.enterPassword("jane.doe80");
        loginComponent.clickLoginButton();

    }

    @When("^Offers page is displayed$")
    public void offersPageIsDisplayed() {
        mainBar = new MainBar(driver);

        // redirect to Offers page
        mainBar.goToOffersPage();

    }

    @Then("^add items met promo conditions to trolley$")
    public void addItemsMetPromoConditionsToTrolleys() {
        List<WebElement> itemsOnDiscountNowPrice = driver.findElements(By.xpath("//li//span[@class='nowPrice']"));
        List<Double> pricesOnDiscountOfFirstThreeProducts;

        // get current prices of first three items on promo
        pricesOnDiscountOfFirstThreeProducts = (Arrays.asList(WebUtils.convertPriceStringToInt(itemsOnDiscountNowPrice.get(0).getText()),
                WebUtils.convertPriceStringToInt(itemsOnDiscountNowPrice.get(1).getText()),
                WebUtils.convertPriceStringToInt(itemsOnDiscountNowPrice.get(2).getText())));


        // add that three items to empty trolley
        List<WebElement> itemsOnDiscountAddButtons = driver
                .findElements(By.xpath("//li[div/div/div/span[@class='wasPrice']]//input[@class='productAdd']"));
        for (int i = 0; i < 3; i++) {
            itemsOnDiscountAddButtons.get(i).click();
        }

        // get total cost in trolley
        Double overallCostInTrolley = WebUtils
                .convertPriceStringToInt(driver.findElement(By.xpath("(" + MAIN_BAR_BASKET_SUMMARY + ")[1]")).getText());

        // check if total cost in trolley is equal to total cost of added items in promo
        assertThat(overallCostInTrolley)
                .isEqualTo(pricesOnDiscountOfFirstThreeProducts.get(0)
                        + pricesOnDiscountOfFirstThreeProducts.get(1)
                        + pricesOnDiscountOfFirstThreeProducts.get(2));

    }
}
