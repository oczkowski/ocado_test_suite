package com.ocado.test;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.TestNGCucumberRunner;
import org.testng.annotations.Test;

/**
 * Created by kuba on 9/7/15.
 */
@CucumberOptions(format = "json:target/cucumber-report-feature-composite.json",
        features = "src/test/resources/com/ocado/test/"/*,
        glue = "src/test/java/com/ocado/test/"*/)
public class RunTest extends TestInit {

    @Test(groups = "", description = "This the launcher")
    public void runCukes() {
        new TestNGCucumberRunner(getClass()).runCukes();
    }
}
